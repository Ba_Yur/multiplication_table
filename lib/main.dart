import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MultiplicationTable(),
    );
  }
}

class MultiplicationTable extends StatelessWidget {

  Widget CreateBox (String text, Color color) {
    return SizedBox(
      height: 35,// here I could use MediaQuery in case I use StateFul widget
      width: 35,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(),
          color: color,
        ),
        // color: color,
        child: Text(text,
        ),
      ),
    );
  }

  Widget CreateTable(int x, int y) {
    List<Widget> rowItems = [];
    List<Widget> columnItems = [];
    for (int i = 0; i <= x; i++) {
      columnItems = [];
      for (int j = 0; j <= x; j++) {
        if (j == 0 && i == 0) {
          columnItems.add(CreateBox('', Colors.green));
        }
        else if(i == 0) {
          columnItems.add(CreateBox((j * 1).toString(), Colors.green));
        }else if (j ==0) {
          columnItems.add(CreateBox((i * 1).toString(), Colors.green));
        }else if (j >=i) {
          columnItems.add(CreateBox((i * j).toString(), Colors.yellow));
        } else {
          columnItems.add(CreateBox((i * j).toString(), Colors.white));
        }
      }
      rowItems.add(Column(
        children: [...columnItems],
      ));
    }
    return Row(
      children: [...rowItems],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Multiplication Table'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: CreateTable(10, 10),
      ),
    );
  }
}
